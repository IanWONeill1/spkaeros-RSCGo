module github.com/spkaeros/rscgo

go 1.13

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/fsnotify/fsnotify v1.4.7
	github.com/gobwas/httphead v0.0.0-20180130184737-2c6c146eadee // indirect
	github.com/gobwas/pool v0.2.0 // indirect
	github.com/gobwas/ws v1.0.2
	github.com/jessevdk/go-flags v1.4.0
	github.com/mattn/anko v0.1.5
	github.com/mattn/go-sqlite3 v2.0.1+incompatible
	github.com/stretchr/testify v1.4.0 // indirect
	go.uber.org/atomic v1.5.1
	golang.org/x/crypto v0.0.0-20191205180655-e7c4368fe9dd
	golang.org/x/lint v0.0.0-20191125180803-fdd1cda4f05f // indirect
	golang.org/x/sys v0.0.0-20191206220618-eeba5f6aabab // indirect
	golang.org/x/tools v0.0.0-20191205225056-3393d29bb9fe // indirect
	gopkg.in/yaml.v2 v2.2.7 // indirect
)
